{
 "cells": [],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 5
}
import pandas as pd 
from sklearn.preprocessing import LabelEncoder
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
import seaborn as sns
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.datasets import make_classification
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.utils import shuffle
import numpy as np
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, balanced_accuracy_score, roc_auc_score, average_precision_score 
from sklearn.model_selection import cross_val_score
from bayes_opt import BayesianOptimization
import time
import matplotlib.pyplot as plt
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import confusion_matrix
from sklearn.neural_network import MLPClassifier
import xgboost as xgb
import psutil
#import resource 
# function for pre-processing the data ---manufacturing machine dataset 
def pre_process_PM(data):
    # mark Random Failure as O in Target 
    data.loc[data['Failure Type']=='Random Failures', 'Target'] = 0 
    # mark Random Failures as No Failure 
    data.loc[data['Failure Type']=='Random Failures', 'Failure Type'] = 'No Failure'
    #label encoder of Type and Failure Type
    le = LabelEncoder()
    # categories follow the alphabetic order
    # high low medium replaced by 0,1,2
    data['Type']= le.fit_transform(data.loc[:,["Type"]].values)
    #No failure (1), Tool wear failure(4), Heat dissipation failure (0), Power failure (2), Overstrain failure (3)  
    data['Failure Type'] = le.fit_transform(data.loc[:,["Failure Type"]].values)
    # create the new variable Heat dissipation
    data['Heat dissipation [K]'] = data['Air temperature [K]'] - data['Process temperature [K]']
    data['Heat dissipation [K]'] = data['Heat dissipation [K]'].abs()
    # create the new variable Power 
    data['Power [W x sec]'] = data['Torque [Nm]'] *( data['Rotational speed [rpm]']* 0.10472)
    # create the new variable Overstrain
    data['Overstrain [min x Nm]']=data['Tool wear [min]']* data['Torque [Nm]']
    # drop useless features 
    data = data.drop(["UDI","Product ID", "Air temperature [K]","Torque [Nm]"],axis = 1)
    #reorder the variable
    data=data[['Type','Power [W x sec]','Process temperature [K]','Heat dissipation [K]','Tool wear [min]','Overstrain [min x Nm]','Rotational speed [rpm]','Target','Failure Type']]
    return (data)
# plausability of fault 
def feat_prob(feature,data):
    x,y = [],[]
    for j in data[feature].unique():
        temp = data
        temp = temp[temp[feature]>=j]
        y.append(round((temp.Target.mean()*100),2))
        x.append(j)
    return(x,y)
# Split training and testing function
# take a dataframe and test_size (0.33, or 0.2 for example)
def split_training_testing(data,test_size,random):
    X  = data.iloc[:, :-2].values
    y  = data.loc[:,['Target','Failure Type']].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size,stratify=y[:,1], random_state=random)
    scaler = MinMaxScaler()
    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    return(X_train, X_test, X_train_scaled, X_test_scaled, y_train, y_test)
# return 6 arrays:
# 0: X_train, 1: X_test, 2: X_train_scaled, 3:X_test_scaled, 4:y_train, 5: y_test
# Gradient Boosting Machine
# regressors:  split[2] for scaled train regressors
# labels: split[4] for labels of the train set

#Function for Performance Metrics micro
# predicted 2D-array containing predicted classes (machine status / Failure type)
# ground_truth 2D-array containing true classes (machine status /Failure type)
# type_avg 'micro' or 'macro'
def performance_failure_type(ground_truth,predicted,type_avg):
    print("Test Precision (Failure Type)      : ",round(precision_score(ground_truth[:,1], predicted[:,1], average=type_avg)*100,2),"%")
    print("Test Recall (Failure Type)         : ",round(recall_score(ground_truth[:,1], predicted[:,1],average=type_avg)*100,2),"%")
    print("Test F1-Score (Failure Type) : ",round(f1_score(ground_truth[:,1], predicted[:,1],average=type_avg)*100,2),"%")
    print("Test Balanced Accuracy Score (Failure Type):",round(balanced_accuracy_score(ground_truth[:,1], predicted[:,1])*100,2),"%")
    return ()
# Performance Metrics Machine Status
# predicted 2D-array containing predicted classes (machine status / Failure type)
# ground_truth 2D-array containing true classes (machine status /Failure type)
def performance_machine_status (ground_truth,predicted):
    print("Test Precision (Machine Status)      : ",round(precision_score(ground_truth[:,0], predicted[:,0])*100,2),"%")
    print("Test Recall (Machine Status)         : ",round(recall_score(ground_truth[:,0], predicted[:,0])*100,2),"%")
    print("Test F1-score (Machine Status) : ",round(f1_score(ground_truth[:,0], predicted[:,0])*100,2),"%")
    return ()
# Confusion Matrix function for machine status and failure type 
# predicted 2D-array containing predicted classes (machine status / Failure type)
# ground_truth 2D-array containing true classes (machine status /Failure type)
# labels : list of labels 
def confusion_machine_status(ground_truth,predicted,labels=['No Failure','Failure']):
    cm = confusion_matrix(ground_truth[:,0],predicted[:,0])
    disp    = ConfusionMatrixDisplay(confusion_matrix = cm, display_labels=labels)
    fig, ax = plt.subplots(figsize = (14,14))
    plt.rcParams.update({'font.size': 14})
    disp.plot(cmap = plt.cm.Greys, ax   = ax)
    plt.xticks(rotation=30, ha='right')
    plt.plot()
    return ()
def confusion_failure_type(ground_truth,predicted,labels=['Heat Dissipation Failure','No Failure','Overstrain Failure','Power Failure','Tool Wear Failure']):
    cm = confusion_matrix(ground_truth[:,1],predicted[:,1])
    disp    = ConfusionMatrixDisplay(confusion_matrix = cm, display_labels=labels)
    plt.rcParams.update({'font.size': 14})
    fig, ax = plt.subplots(figsize = (32,24))
    disp.plot(cmap = plt.cm.Greys, ax   = ax)
    plt.xticks(rotation=30, ha='right')
    plt.yticks(rotation=30,ha='right')
    plt.plot()
    return ()
# pre process function for electrical fault 
# definition of failure type variable and machine status variable. 
def pre_process_EF(data):
# define a function for failure type classification in a unique variable    
    def conditions_FT(s):
        if (s['G']== 1) and (s['C'] == 0) and (s['B'] == 0) and (s['A']==1):
            return "Fault between phase A and ground"
        elif (s['G']== 0) and (s['C'] == 1) and (s['B'] == 1) and (s['A']==0):
            return "Fault between phase B and phase C"
        elif (s['G']== 1) and (s['C'] == 0) and (s['B'] == 1) and (s['A']==1):
            return "Fault between phases A,B and ground"
        elif (s['G']== 0) and (s['C'] == 1) and (s['B'] == 1) and (s['A']==1):
            return "Fault between all three phases"
        elif (s['G']== 1) and (s['C'] == 1) and (s['B'] == 1) and (s['A']==1):
            return "Three phase symmetrical fault"
        else: 
            return "No failure"
    data['Failure Type'] = data.apply(conditions_FT, axis=1)
# define a function for machibe status classification 
    def conditions_MS(s):
        if (s['Failure Type']== "No failure"):
            return 0
        else: 
            return 1
    data['Target'] = data.apply(conditions_MS, axis=1)    
    data= data.drop(["G","C", "B","A"],axis = 1)
    data=data[["Ia","Ib","Ic","Va","Vb","Vc","Target","Failure Type"]]
    return (data)
#### Functions for Bayesian optimization with stratified cross validation 
### gradient boost classifier 
def grid_gbm_cv (regressors,labels,obj):
    # function for the maximization of the target
    def gbm_cl_bo(max_depth, max_features, learning_rate, n_estimators, subsample,min_samples_leaf,min_samples_split):
        params_gbm = {}
        params_gbm['max_depth'] = int(max_depth)
        params_gbm['max_features'] = max_features
        params_gbm['learning_rate'] = learning_rate
        params_gbm['n_estimators'] = int(n_estimators)
        params_gbm['subsample'] = subsample
        params_gbm['min_samples_leaf']=int(min_samples_leaf)
        params_gbm['min_samples_split']=int(min_samples_split)
        classifier = GradientBoostingClassifier(random_state=123, **params_gbm)
        scores=cross_val_score(classifier,regressors,labels[:,1],cv=5,scoring=obj,n_jobs=-1)
        target=scores.mean()
        return (target)
    # Candidates
    params_gbm ={
        'max_depth':(2, 10),
        'max_features':(0.8, 1),
        'learning_rate':(0.08,0.16),
        'n_estimators':(64, 256),
        'subsample': (0.8, 1),
        'min_samples_leaf':(4,8),
        'min_samples_split':(6,16)
    }
    gbm_bo = BayesianOptimization(gbm_cl_bo, params_gbm, random_state=111,verbose=1)
    gbm_bo.maximize(init_points=20, n_iter=5)
    params_gbm = gbm_bo.max['params']
    params_gbm['max_depth'] = int(params_gbm['max_depth'])
    params_gbm['n_estimators'] = int(params_gbm['n_estimators'])
    params_gbm['max_features'] = params_gbm['max_features']
    params_gbm['subsample'] = params_gbm['subsample']
    params_gbm['learning_rate'] = params_gbm['learning_rate']
    params_gbm['min_samples_leaf'] = int(params_gbm['min_samples_leaf'])
    params_gbm['min_samples_split'] = int(params_gbm['min_samples_split'])
    print(params_gbm)
    return(params_gbm) 
### Adaboost classifier
def grid_abm_cv(regressors,labels,obj):
    # function for the maximization of the target
    def abm_cl_bo(learning_rate, n_estimators):
        params_abm = {}
        params_abm['learning_rate'] = learning_rate
        params_abm['n_estimators'] = int(n_estimators)
        classifier = AdaBoostClassifier(random_state=123, **params_abm)
        scores=cross_val_score(classifier,regressors,labels[:,1],cv=5,scoring=obj,n_jobs=-1)
        target=scores.mean()
        return target
    # set of possible candidates
    params_abm ={
        'learning_rate':(0.08,0.16),
        'n_estimators':(64, 256),
    }
    abm_bo = BayesianOptimization(abm_cl_bo, params_abm, random_state=111,verbose=1)
    abm_bo.maximize(init_points=20, n_iter=5)
    params_abm = abm_bo.max['params']
    params_abm['n_estimators'] = int(params_abm['n_estimators'])
    params_abm['learning_rate'] = params_abm['learning_rate']
    print(params_abm)
    return(params_abm)
### Random forest classifier 
# bayes opt grid search
def grid_rf_cv (regressors,labels,obj):
    # grid for the quality of the split 
    criteria=['gini', 'entropy', 'log_loss'] # 0,1,2
    number_features=['sqrt','log2',None]
    #function for the maximization of the target
    def rf_cl_bo(max_depth, max_features,n_estimators,criterion,min_sample_leaf,min_sample_split,max_samples):
        params_rf={}
        params_rf['max_depth'] = int(max_depth)
        params_rf['max_features'] = number_features[int(max_features)]
        params_rf['criterion']=criteria[int(criterion)]
        params_rf['n_estimators'] = int(n_estimators)
        params_rf['min_sample_leaf']=int(min_sample_leaf)
        params_rf['min_sample_split']=int(min_sample_split)
        params_rf['max_samples']=max_samples
        classifier = RandomForestClassifier(random_state=123,criterion=params_rf['criterion'],n_estimators=params_rf['n_estimators'],max_depth=params_rf['max_depth'],min_samples_leaf=params_rf['min_sample_leaf'],max_features=params_rf['max_features'],max_samples=params_rf['max_samples'],min_samples_split=params_rf['min_sample_split'])
        scores=cross_val_score(classifier,regressors,labels[:,1],cv=5,scoring=obj,n_jobs=-1)
        target=scores.mean()
        return (target)
    params_rf ={
        'max_depth':(2, 10),
        'max_features':(0,2.99),
        'n_estimators':(64,256), 
        'criterion':(0,2.99), # int 0,1,2
        'min_sample_leaf':(4,8),
        'min_sample_split':(6,16),
        'max_samples':(0.1,1)
    }
    rf_bo = BayesianOptimization(rf_cl_bo, params_rf, random_state=111,verbose=1)
    rf_bo.maximize(init_points=25, n_iter=5)
    print(rf_bo.max)
    params_rf = rf_bo.max['params']
    params_rf={}
    params_rf['n_estimators']= int(rf_bo.max["params"]["n_estimators"])
    params_rf["max_features"]=number_features[int(rf_bo.max["params"]["max_features"])]
    params_rf["max_depth"] = int(rf_bo.max["params"]["max_depth"])
    params_rf['criterion']= criteria[int(rf_bo.max["params"]["criterion"])]
    params_rf['min_sample_leaf']=int(rf_bo.max['params']['min_sample_leaf'])
    params_rf['min_sample_split']=int(rf_bo.max['params']['min_sample_split'])
    params_rf['max_samples']=rf_bo.max['params']['max_samples']
    #print(params_tree)
    return (params_rf)

#### XGBoost
def grid_xgb_cv (regressors,labels,obj):
    #function for the maximization of the target
    def xgb_cl_bo(max_depth,n_estimators,colsample_bytree,eta,gamma,min_child_weight):
        params_xgb={}
        params_xgb['max_depth'] = int(max_depth)
        params_xgb['n_estimators'] = int(n_estimators)
        params_xgb['colsample_bytree']=colsample_bytree
        params_xgb['min_child_weight'] = int(min_child_weight)
        params_xgb['eta']=eta
        params_xgb['gamma']=gamma
        classifier = xgb.XGBClassifier(random_state=123,n_estimators=params_xgb['n_estimators'],max_depth=params_xgb['max_depth'],gamma=params_xgb['gamma'],eta=params_xgb['eta'],colsample_bytree=params_xgb['colsample_bytree'],min_child_weight=params_xgb['min_child_weight'])
        scores=cross_val_score(classifier,regressors,labels[:,1],cv=5,scoring=obj,n_jobs=-1)
        target=scores.mean()
        return (target)
    params_xgb ={
        'max_depth':(2, 10),
        'n_estimators':(64,256), 
        'min_child_weight':(8,16), 
        'gamma':(0.6,1.2),
        'eta':(0.08,0.16),
        'colsample_bytree':(0.1,0.3)
    }
    xgb_bo = BayesianOptimization(xgb_cl_bo, params_xgb, random_state=111,verbose=1)
    xgb_bo.maximize(init_points=25, n_iter=5)
    print(xgb_bo.max)
    params_xgb = xgb_bo.max['params']
    params_xgb={}
    params_xgb['n_estimators']= int(xgb_bo.max["params"]["n_estimators"])
    params_xgb["max_depth"] = int(xgb_bo.max["params"]["max_depth"])
    params_xgb['min_child_weight']= int(xgb_bo.max["params"]["min_child_weight"])
    params_xgb['eta']=xgb_bo.max['params']['eta']
    params_xgb['gamma']=xgb_bo.max['params']['gamma']
    params_xgb['colsample_bytree']=xgb_bo.max['params']['colsample_bytree']
    #print(params_tree)
    return (params_xgb) 

### MultiLayer Perceptron 
def grid_nn_ef_cv(regressors,labels,obj):
    #lbfgs
    solver=['lbfgs','sgd', 'adam']
    activation=['identity', 'logistic', 'tanh', 'relu']
    learning_rate=['constant', 'invscaling', 'adaptive']
    early_stopping=[True,False]
    # maximize the target
    def nn_cl_bo(neurons1, activation_function, optimizer, initial_learning_rate, batch_size,
               momentum, validation_fraction, early_stop,alpha,max_iter):
        params_nn={}
        params_nn['batch_size'] = int(batch_size)
        params_nn['validation_fraction']=validation_fraction
        params_nn['momentum']=momentum
        params_nn['solver']=solver[int(optimizer)]
        params_nn['activation']=activation[int(activation_function)]
        params_nn['learning_rate']=learning_rate[int(initial_learning_rate)]
        params_nn['early_stopping']=early_stopping[int(early_stop)]
        params_nn['max_iter']=int(max_iter)
        params_nn['alpha']=alpha
        params_nn['hidden_layer_sizes']=(int(neurons1),)
        classifier = MLPClassifier(random_state=123, **params_nn)
        scores=cross_val_score(classifier,regressors,labels[:,1],cv=5,scoring=obj,n_jobs=-1)
        target=scores.mean()
        return (target)
    # set of possible candidates
    params_nn ={
        'validation_fraction':(0,0.99),
        'momentum':(0,1),
        'batch_size':(16,128),
        'early_stop':(0,1.99), # int 0,1
        'initial_learning_rate':(0,2.99) ,# int 0,1,2,
        'activation_function':(0,3.99), # int 0,1,2,3
        'optimizer':(0,2.99), # int 0,1
        'neurons1':(int(1.5*regressors.shape[1]),int(5/2*regressors.shape[1])),
        'max_iter':(1000000,1100000),
        'alpha':(0.00001,0.01)
    }
    # bayes optimization
    nn_bo = BayesianOptimization(nn_cl_bo, params_nn, random_state=111,verbose=1)
    nn_bo.maximize(init_points=20, n_iter=5)
    params_nn={}
    params_nn["activation"]= activation[int(nn_bo.max["params"]["activation_function"])]
    params_nn["momentum"] = nn_bo.max["params"]["momentum"]
    params_nn["validation_fraction"]=nn_bo.max["params"]["validation_fraction"]
    params_nn["batch_size"] = int(nn_bo.max["params"]["batch_size"])
    params_nn["learning_rate"]= learning_rate[int(nn_bo.max["params"]["initial_learning_rate"])]
    params_nn["solver"]= solver[int(nn_bo.max["params"]["optimizer"])]
    params_nn["early_stopping"]= early_stopping[int(nn_bo.max["params"]["early_stop"])]
    params_nn["max_iter"]= int(nn_bo.max["params"]["max_iter"])
    params_nn["alpha"]= nn_bo.max["params"]["alpha"]
    params_nn["hidden_layer_sizes"]=(int(nn_bo.max["params"]["neurons1"]),)
    print(params_nn)
    return (params_nn)  
# evaluation of random forest 
def evaluation_rf(database,obj): 
    # define a list for the evaluation metrics 
    evaluation=['precision','recall','f1','time']
    # initialize the dataframe
    eval = pd.DataFrame(data=None, index=evaluation)
    random_split=[123,333,567,999,876,371,459,111,902,724]
    for i in range(0,10):
        # time 
        start=time.process_time()
        # memory in MB 
        start_memory=psutil.Process().memory_info().rss / (1024 * 1024) 
        #split
        split_=split_training_testing(database, test_size=0.33, random=random_split[i])
        # optimization of the hyperparameters with cross-validation in the train set 
        train_rf_=grid_rf_cv(split_[2], split_[4],obj)
        # fit the model with the best hyperparameters 
        classifier = RandomForestClassifier(random_state=123,criterion=train_rf_['criterion'],n_estimators=train_rf_['n_estimators'],max_depth=train_rf_['max_depth'],max_features=train_rf_['max_features'],min_samples_leaf=train_rf_['min_sample_leaf'],min_samples_split=train_rf_['min_sample_split'],max_samples=train_rf_['max_samples'],n_jobs=-1)
        classifier.fit(split_[2], split_[4][:,1])
        #predict on test data class labels  
        y_pred_test_=classifier.predict(split_[3])
        # array for features,precison,recall,f1
        recall=np.append(round(precision_score(split_[5][:,1], y_pred_test_,average='macro')*100,2),round(recall_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        f1=np.append(recall,round(f1_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        metrics=np.append(f1, (time.process_time() - start))
        #metrics=np.append(time_,psutil.Process().memory_info().rss / (1024 * 1024)-start_memory)
        eval[i]=metrics.tolist()
    # replace 0, with NaN 
    eval=eval.replace(0,np.nan)
    mean=eval.mean(axis=1)
    std=eval.std(axis=1)
    result=pd.concat([mean, std], axis=1)
    return(result)

# evaluation xgboost 
def evaluation_xgb(database,obj): 
    # define a list for the evaluation metrics 
    evaluation=['precision','recall','f1','time']
    # initialize the dataframe
    eval = pd.DataFrame(data=None, index=evaluation)
    random_split=[123,333,567,999,876,371,459,111,902,724]
    for i in range(0,10):
        # time 
        start=time.process_time()
        # memory in MB 
        start_memory=psutil.Process().memory_info().rss / (1024 * 1024) 
        #split
        split_=split_training_testing(database, test_size=0.33, random=random_split[i])
        # optimization of the hyperparameters with cross-validation in the train set 
        train_rf_=grid_xgb_cv(split_[2], split_[4],obj)
        # fit the model with the best hyperparameters 
        classifier = xgb.XGBClassifier(random_state=123,n_estimators=train_rf_['n_estimators'],max_depth=train_rf_['max_depth'],gamma=train_rf_['gamma'],eta=train_rf_['eta'],colsample_bytree=train_rf_['colsample_bytree'],min_child_weight=train_rf_['min_child_weight'],n_jobs=-1)
        classifier.fit(split_[2], split_[4][:,1])
        #predict on test data class labels  
        y_pred_test_=classifier.predict(split_[3])
        # array for features,precison,recall,f1
        recall=np.append(round(precision_score(split_[5][:,1], y_pred_test_,average='macro')*100,2),round(recall_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        f1=np.append(recall,round(f1_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        metrics=np.append(f1, (time.process_time() - start))
        #metrics=np.append(time_,psutil.Process().memory_info().rss / (1024 * 1024)-start_memory)
        eval[i]=metrics.tolist()
    # replace 0, with NaN 
    eval=eval.replace(0,np.nan)
    mean=eval.mean(axis=1)
    std=eval.std(axis=1)
    result=pd.concat([mean, std], axis=1)
    return(result)

# evaluation neural network 
def evaluation_nn(database,obj): 
    # define a list for the evaluation metrics 
    evaluation=['precision','recall','f1','time']
    # initialize the dataframe
    eval = pd.DataFrame(data=None, index=evaluation)
    random_split=[123,333,567,999,876,371,459,111,902,724]
    for i in range(0,10):
        # time 
        start=time.process_time()
        # memory in MB 
        start_memory=psutil.Process().memory_info().rss / (1024 * 1024) 
        #split
        split_=split_training_testing(database, test_size=0.33, random=random_split[i])
        # optimization of the hyperparameters with cross-validation in the train set 
        train_rf_=grid_nn_ef_cv(split_[2], split_[4],obj)
        # fit the model with the best hyperparameters 
        classifier = MLPClassifier(random_state=123,**train_rf_)
        classifier.fit(split_[2], split_[4][:,1])
        #predict on test data class labels  
        y_pred_test_=classifier.predict(split_[3])
        # array for features,precison,recall,f1
        recall=np.append(round(precision_score(split_[5][:,1], y_pred_test_,average='macro')*100,2),round(recall_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        f1=np.append(recall,round(f1_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        metrics=np.append(f1, (time.process_time() - start))
        #metrics=np.append(time_,psutil.Process().memory_info().rss / (1024 * 1024)-start_memory)
        eval[i]=metrics.tolist()
    # replace 0, with NaN 
    eval=eval.replace(0,np.nan)
    mean=eval.mean(axis=1)
    std=eval.std(axis=1)
    result=pd.concat([mean, std], axis=1)
    return(result)

## adaptive boosting 
def evaluation_ada(database,obj): 
    # define a list for the evaluation metrics 
    evaluation=['precision','recall','f1','time']
    # initialize the dataframe
    eval = pd.DataFrame(data=None, index=evaluation)
    random_split=[123,333,567,999,876,371,459,111,902,724]
    for i in range(0,10):
        # time 
        start=time.process_time()
        # memory in MB 
        start_memory=psutil.Process().memory_info().rss / (1024 * 1024) 
        #split
        split_=split_training_testing(database, test_size=0.33, random=random_split[i])
        # optimization of the hyperparameters with cross-validation in the train set 
        train_rf_=grid_abm_cv(split_[2], split_[4],obj)
        # fit the model with the best hyperparameters 
        classifier = AdaBoostClassifier(random_state=123,**train_rf_)
        classifier.fit(split_[2], split_[4][:,1])
        #predict on test data class labels  
        y_pred_test_=classifier.predict(split_[3])
        # array for features,precison,recall,f1
        recall=np.append(round(precision_score(split_[5][:,1], y_pred_test_,average='macro')*100,2),round(recall_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        f1=np.append(recall,round(f1_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        metrics=np.append(f1, (time.process_time() - start))
        #metrics=np.append(time_,psutil.Process().memory_info().rss / (1024 * 1024)-start_memory)
        eval[i]=metrics.tolist()
    # replace 0, with NaN 
    eval=eval.replace(0,np.nan)
    mean=eval.mean(axis=1)
    std=eval.std(axis=1)
    result=pd.concat([mean, std], axis=1)
    return(result)

## gradient boosting
def evaluation_gb(database,obj): 
    # define a list for the evaluation metrics 
    evaluation=['precision','recall','f1','time']
    # initialize the dataframe
    eval = pd.DataFrame(data=None, index=evaluation)
    random_split=[123,333,567,999,876,371,459,111,902,724]
    for i in range(0,10):
        # time 
        start=time.process_time()
        # memory in MB 
        start_memory=psutil.Process().memory_info().rss / (1024 * 1024) 
        #split
        split_=split_training_testing(database, test_size=0.33, random=random_split[i])
        # optimization of the hyperparameters with cross-validation in the train set 
        train_rf_=grid_gbm_cv(split_[2], split_[4],obj)
        # fit the model with the best hyperparameters 
        classifier = GradientBoostingClassifier(random_state=123,**train_rf_)
        classifier.fit(split_[2], split_[4][:,1])
        #predict on test data class labels  
        y_pred_test_=classifier.predict(split_[3])
        # array for features,precison,recall,f1
        recall=np.append(round(precision_score(split_[5][:,1], y_pred_test_,average='macro')*100,2),round(recall_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        f1=np.append(recall,round(f1_score(split_[5][:,1], y_pred_test_,average='macro')*100,2))
        metrics=np.append(f1, (time.process_time() - start))
        #metrics=np.append(time_,psutil.Process().memory_info().rss / (1024 * 1024)-start_memory)
        eval[i]=metrics.tolist()
    # replace 0, with NaN 
    eval=eval.replace(0,np.nan)
    mean=eval.mean(axis=1)
    std=eval.std(axis=1)
    result=pd.concat([mean, std], axis=1)
    return(result)


        
    
        

        

    
    